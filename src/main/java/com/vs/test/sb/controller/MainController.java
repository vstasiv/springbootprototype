package com.vs.test.sb.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
	@RequestMapping("/")
	public String sayHell() {
		return "Hello Viktor. It's a prototype of spring boot application";
	}
}
