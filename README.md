It is a simple prototype application using spring boot.

**Prerequirements**

* Java 8 or later
* Maven 3.2+

**How to run:**

1. git clone https://vstasiv@bitbucket.org/vstasiv/springbootprototype.git
2. cd springbootprototype
3. mvn spring-boot:run

** How to check it:**

* Just open browser and go to localhost:8080 and you should see hello message